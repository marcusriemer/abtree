#!/usr/bin/python

import sys
import random

from collections import defaultdict

random.seed()

def printTestcase(num):
    dict = defaultdict(lambda: 0, {})
    print("create,2,3")
    print("echo,{},insert,".format(num))
    print("timer,start")
    for i in range(num):
       key = random.randint(0, num / 2);
       dict[key] += 1;
       print("insert,{},{}".format(key, dict[key]))
    print("timer,print")

    print("echo,{},visit,".format(num))
    print("timer,start")
    print("size")
    print("timer,print")

    print("echo,{},lookup,".format(num))
    print("timer,start")

    for i in range(num):
        print("contains,{},{}".format(i, dict[i] != 0))

    print("timer,print")


#printTestcase(10)
printTestcase(1000)
printTestcase(5000)
printTestcase(10000)
printTestcase(50000)
printTestcase(100000)
printTestcase(500000)
#printTestcase(1000000)
#printTestcase(5000000)
