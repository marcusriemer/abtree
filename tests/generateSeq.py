#!/usr/bin/python

import sys

A = 2 if len(sys.argv) < 2 else int(sys.argv[1])
B = A + 1 if len(sys.argv) < 3 else int(sys.argv[2])

def printTestcase(num):
    print("create,{},{}".format(A, B))
    print("echo,{},insert,".format(num))
    print("timer,start")
    for i in range(num):
        key = i;
        print("insert,{},{}".format(key, 1))
    print("timer,print")

    print("echo,{},visit,".format(num))
    print("timer,start")
    print("size")
    print("timer,print")

    print("echo,{},lookup,".format(num))
    print("timer,start")

    for i in range(num):
        print("contains,{},{}".format(i, True))

    print("timer,print")

#printTestcase(1000)
#printTestcase(5000)
#printTestcase(10000)
printTestcase(10000)
printTestcase(20000)
printTestcase(30000)
printTestcase(40000)
printTestcase(50000)
printTestcase(60000)
printTestcase(70000)
printTestcase(80000)
printTestcase(90000)
printTestcase(100000)
#printTestcase(500000)
#printTestcase(1000000)
#printTestcase(5000000)
