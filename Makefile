csvdata=$(shell find analysis -iname "*.csv")
pdfdata=$(csvdata:csv=pdf)
pngdata=$(csvdata:csv=png)

all :
	rm -rf build/*
	mkdir -p build
	cd build; cmake ./../src ; make --no-print-directory

allDebug:
	cd build; cmake "-DCMAKE_BUILD_TYPE:STRING=Debug" ./../src; make --no-print-directory

run : 
	@build/abtree

clean :
	rm -rf ./build/*
	rm -rf ./doc/*
	rm -f $(pdfdata)

analysis : $(pdfdata) $(pngdata) analysis/graph.r

analysis.pdf : $(pdfdata) analysis/graph.r
	pdftk analysis/*.seq.pdf cat output analysis/analysis.pdf

%.pdf:%.csv
	cd analysis && Rscript graph.r pdf $(shell basename $^ .csv)

%.png:%.csv
	cd analysis && Rscript graph.r png $(shell basename $^ .csv)

doc :
	doxygen

.PHONY : all allDebug run clean doc
