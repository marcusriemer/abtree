library(lattice)

args <- commandArgs(trailingOnly = TRUE)
fileName <- args[2]
fileType <- args[1]
x <- read.table(paste(fileName, ".csv", sep=""), sep=",", header=T)
trellis.device(device=fileType, file=paste(fileName, fileType, sep="."))
xyplot(time ~ size | person, data=x, groups=op, type="b", scale=list(x = list(log = 10), y = list(log = 10)))
dev.off()
