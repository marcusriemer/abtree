#ifndef ABTREE_HPP
#define ABTREE_HPP

#include <cstddef>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <iostream>
 
enum class SizeType
{
  Leafs,
  LeafsAndNodes
};

// Forward declaration of Nodes
template< typename TKey, typename TValue >
class abNode;

/*!
 * \brief Contains various properties of an abTree
 *
 */
struct abTreeStats
{
  abTreeStats()
    : numLeafs(0)
    , numInnerNodes(0)
  {

  }

  std::size_t numLeafs;

  std::size_t numInnerNodes;
};

/*!
 * Acts as a virtual root node for abTrees, providing all the typical
 * and relevant tree operations.
 */
template< typename TKey, typename TValue >
class abTree
{
private:
  //! Type of child nodes
  typedef abNode<TKey, TValue> Node;

  //! Type of this tree itself
  typedef abTree<TKey, TValue> Tree;

  //! The container used to store children
  typedef std::vector<Node*> ChildContainer;

  //! The iterator used to iterate over stored children
  typedef typename ChildContainer::iterator ChildIterator;

public:
  /*!
   * \brief Standard Ctor is explictly forbidden
   * There is no way to define an ab Tree without knowing what values
   * to use for a and b.
   */
  abTree() = delete;

  /*!
   * \brief Construct an empty ab Tree
   * \param a Lower bound regarding the number of children
   * \param b Upper bound regarding the number of children
   */
  abTree( const std::size_t a, const std::size_t b )
    : A( a )
    , B( b )
  {
    mChildren.reserve( B );
  }

  /*!
   * \brief Clears the entire tree on teardown
   */
  ~abTree()
  {
    clear();
  }

  /*!
   * \brief Checks whether a certain key is part of the tree
   * \param key The key to check
   * \return True, if the key is stored in a leaf
   */
  bool contains( const TKey& key )
  {
    // We delegate all work to those children of ours
    for( Node* node : mChildren )
    {
      if( node->contains( key ) )
      {
        return ( true );
      }
    }

    return ( false );
  }

  /*!
   * \brief insert Upserts a new value at the given key position
   * Upsert is basically insert if the key doesn't exist, update
   * if it exists.
   * \param key The key to upsert at
   * \param value The value to upsert
   */
  void insert( const TKey& key, const TValue& value )
  {
    // Construct the actual new node
    Node* node = new Node( this, key, value );

    // Okay, it's not so easy. Lets look where whe should insert
    auto insertPos = getSubtreeIndex( mChildren, key );

    // Easiest case: There is absolutly no node yet
    if( mChildren.empty() )
    {
      mChildren.insert( insertPos, node );
      return;
    }

    // Never read past the greatest element when recursively inserting
    if( insertPos == mChildren.end() )
    {
      insertPos--;
    }

    // And let those children do the work
    ChildContainer newChildren = ( *insertPos )->insert( node );

    // Only one child returned -> New root
    if( newChildren.size() == 1 )
    {
      if( *insertPos != newChildren.back() )
      {
        delete *insertPos;
        *insertPos = newChildren.back();
      }
    }
    else if( newChildren.size() == 2 )
    {
      // Check whether the node we originated at is no longer part of the
      // tree. In this case we should delete it.
      auto oldNodeContained = std::find( newChildren.begin(), newChildren.end(), *insertPos );
      if( oldNodeContained == newChildren.end() )
      {
        delete *insertPos;
        insertPos = mChildren.erase( insertPos );
      }

      // Insert those new Children, but make sure we are not
      // inserting an existing child again.
      for( Node* toInsert : newChildren )
      {
        auto childPos = getSubtreeIndex( mChildren, toInsert->getKey() );
        if( oldNodeContained == newChildren.end() ||
          childPos == mChildren.end() ||
          *insertPos != toInsert )
        {
          mChildren.insert( childPos, toInsert );
        }
      }

      // And possibly do a split in case we forgot something
      if( mChildren.size() > B )
      {
        divideSubtree( this, mChildren );
      }
    }
  }

  void erase( const TKey& key )
  {
    // Easiest case: Empty tree
    if( mChildren.empty() )
    {
      return;
    }

    // Let's start to look for the correct subtree
    auto searchPos = getSubtreeIndex( mChildren, key );

    // Still an easy case: We immediatly found a leaf
    if( searchPos != mChildren.end() && ( *searchPos )->isLeaf() )
    {
      delete *searchPos;
      mChildren.erase( searchPos );
      return;
    }

    ChildContainer newChildren = ( *searchPos )->erase( key );
  }

  void clear()
  {
    for (Node* node : mChildren)
    {
      // Leafs don't have any dynamically alloctated children, so we
      // can safely skip deletion of those.
      if (node->isLeaf())
      {
        node->clear();
      }
      delete node;
    }
  }

  /*!
  * \brief The size of this tree
  * \return Any number >= 0, depending on the size of the tree
  */
  const abTreeStats stats( )
  {
    abTreeStats toReturn;
    for( Node* node : mChildren )
    {
      node->stats( toReturn );
    }

    return ( toReturn );
  }

  /*!
   * \brief Prints the elements of the tree on the given stream
   * \param out A writeable stream
   */
  void print( std::ostream& out )
  {
    for( Node* node : mChildren )
    {
      node->print( out, 0 );
    }
  }

private:
  /*!
   * \brief Calculates the index new nodes should be inserted
   * \param children Children receiving a new node
   * \param key The key to insert at
   * \return An iterator specifying the insertion position
   */
  static ChildIterator getSubtreeIndex( ChildContainer& children,
    const TKey& key )
  {
    // Find lowest key of children
    auto comp = [ &key, &children ]( const Node* rhs ) {
      return ( key <= rhs->getKey() );
    };

    auto pos = std::find_if( children.begin(), children.end(), comp );
    return ( pos );
  }

  /*!
   * \brief Splits the given nodes up into two parent nodes and appends these
   * \param children The parent tree to possibly split
   */
  static void divideSubtree( Tree* tree, ChildContainer& children )
  {
    // And possibly do a split
    auto itBegin = children.begin();
    auto itMiddle = children.begin() + children.size() / 2;
    auto itEnd = children.end();

    Node* lhs = new Node( tree, itBegin, itMiddle );
    Node* rhs = new Node( tree, itMiddle, itEnd );

    children.clear();
    children.push_back( lhs );
    children.push_back( rhs );
  }

  /*!
   * \brief Used to delete inner nodes that may have become obsolete
   * \param
   */
  static ChildIterator deleteObsoleteNode( ChildContainer& children,
    ChildContainer& newNodes,
    ChildIterator entryNode )
  {
    auto entryNodeNewPos = std::find( newNodes.begin(), newNodes.end(), *entryNode );
    if( entryNodeNewPos == newNodes.end() )
    {
      delete *entryNode;
      return( children.erase( entryNode ) );
    }
    else
    {
      return ( entryNode );
    }
  }
public:
  const std::size_t A;
  const std::size_t B;


private:
  /*!
   * \brief Checks the invariant regarding the bounds of a and b
   */
  void invParams()
  {
    if( !( 2 <= A ) || !( A <= ( B + 1 ) / 2 ) )
    {
      throw std::runtime_error( "ab invariant violated" );
    }
  }


private:
  //! Children of the root node
  ChildContainer mChildren;

  //! Nodes and Trees are coupled very tightly ...
  friend class abNode < TKey, TValue > ;

};

/*!
 * A node of an ab Tree
 */
template< typename TKey, typename TValue >
class abNode
{
private:
  typedef abTree<TKey, TValue> Tree;

  typedef typename Tree::Node Node;

  typedef typename Tree::ChildContainer ChildContainer;

  typedef typename ChildContainer::iterator ChildIterator;

public:
  abNode( Tree *tree, ChildIterator begin, ChildIterator end )
    : mTree( tree )
    , mIsLeaf( false )
    , mChildren( begin, end )
    , mKey( mChildren.back()->mKey )
  {
    ++NodeCount;
  }

  /*!
   * \brief Leaf node constructor
   * \param tree The parenting tree
   * \param key The key to store
   * \param value The value to store
   */
  abNode( Tree* tree, const TKey& key, const TValue& value )
    : mTree( tree )
    , mIsLeaf( true )
    , mKey( key )
    , mValue( value )
  {
    ++NodeCount;
  }

  /*!
   *
   */
  ~abNode()
  {
    --NodeCount;
  }

  /*!
   * \brief Checks whether this node or any child contains the given key
   * \param key The key to check
   * \return True, if this subtree contains the key
   */
  bool contains( const TKey& key )
  {
    // Easiest case first: This is a leaf
    if( mIsLeaf )
    {
      // And maybe it's the key we are looking for
      return ( mKey == key );
    }
    // Okay, this isn't a leaf, time to ask those children to do some work
    else
    {
      auto pos = Tree::getSubtreeIndex( mChildren, key );

      if( pos == mChildren.end() )
      {
        --pos;
      }

      return ( ( *pos )->contains( key ) );
    }
  }

  ChildContainer insert( typename Tree::Node* node )
  {
    ChildContainer toReturn;

    if( mIsLeaf )
    {
      toReturn.push_back( this );

      // Can we do a simple update which doesn't require new nodes?
      if( mKey == node->mKey )
      {
        // Yes, thats easy
        mValue = node->mValue;
      }
      else
      {
        // No, tell the caller to look somewhere else to insert this node
        toReturn.push_back( node );
      }
    }
    else
    {
      auto pos = Tree::getSubtreeIndex( mChildren, node->mKey );

      if( pos == mChildren.end() )
      {
        --pos;
      }

      ChildContainer newChildren = ( *pos )->insert( node );

      // If only one root is returned, adopt this as new child (instead
      // of the former one) and return own root.
      if( newChildren.size() == 1 && *pos != newChildren.back() )
      {
        throw std::runtime_error( "Should this happen?" );
        delete *pos;
        *pos = newChildren.back();
        toReturn.push_back( this );
      }
      // If two roots are returned and only one more child exists, adopt
      // both roots as new children and return own root.
      else if( newChildren.size() == 2 )
      {
        // Check whether the node we originated at is no longer part of the
        // tree. In this case we should delete it.
        auto oldNodeContained = std::find( newChildren.begin(), newChildren.end(), *pos );
        if( oldNodeContained == newChildren.end() )
        {
          delete *pos;
          mChildren.erase( pos );
        }

        for( Node* toInsert : newChildren )
        {
          auto childPos = Tree::getSubtreeIndex( mChildren, toInsert->getKey() );
          if( childPos == mChildren.end() || *childPos != toInsert )
          {
            mChildren.insert( childPos, toInsert );
          }
        }

        // If two roots are returned and two more other children exist
        // (resulting in four new children), split root node into 2 nodes,
        // distribute the children such that each node has 2 of them and
        // return the references to the nodes of the split.
        if( mChildren.size() > mTree->B )
        {
          Tree::divideSubtree( mTree, mChildren );
          toReturn = mChildren;
        }
        else
        {
          toReturn.push_back( this );
        }
      }
    }

    return ( toReturn );
  }

  ChildContainer erase( const TKey& key )
  {
    ChildContainer toReturn;

    // If the children of root are leaves, return those children not containing 
    // data with the searched key.
    if( mIsLeaf )
    {
      if( mKey == key )
      {
        toReturn.push_back( this );
      }
    }
    else
    {
      auto searchPos = Tree::getSubtreeIndex( mChildren, key );

      if( searchPos != mChildren.end() )
      {
        toReturn = mChildren;
      }
      else
      {
        ChildContainer sub = ( *searchPos )->erase( key );
      }
    }

    return ( toReturn );
  }

  /*!
   * \brief Erases and clears the whole subtree
   */
  void clear()
  {
    if (mIsLeaf)
    {
      throw std::runtime_error("Called clear() on leaf node");
    }

    for (Node* node : mChildren)
    {
      // Leafs don't have any dynamically alloctated children, so we
      // can safely skip deletion of those.
      if (node->mIsLeaf)
      {
        node->clear();
      }
      delete node;
    }

    mChildren.clear();
  }

  /*!
   * \brief Increments size when called on a leaf, delegates to children on inner node
   * \param akk The akkumulator
   */
  void stats( abTreeStats& stats )
  {
    if( mIsLeaf )
    {
      stats.numLeafs += 1;
    }
    else
    {
      stats.numInnerNodes += 1;

      for( typename Tree::ChildContainer::value_type node : mChildren )
      {
        node->stats( stats );
      }
    }
  }

  /*!
   * \brief Print this tree at a certain depth
   * \param out The stream that should be used to print on
   * \param level Level of indentation
   */
  void print( std::ostream& out, std::size_t level )
  {
    if( mIsLeaf )
    {
      out << std::string( level, ' ' ) << mKey << " => " << mValue << std::endl;
    }
    else
    {
      out << std::string( level, ' ' ) << "Inner => " << mKey << std::endl;
      for( typename Tree::ChildContainer::value_type node : mChildren )
      {
        node->print( out, level + 1 );
      }
    }
  }

  /*!
   * \brief Retrieve the maximum value that exists somewhere in this subtree
   */
  const TKey& getMaxKey() const
  {
    if( mIsLeaf )
    {
      return ( mKey );
    }
    else
    {
      if( mChildren.empty() )
      {
        throw std::runtime_error( "getMaxKey() on empty subtree" );
      }

      return ( mChildren.back()->getMaxKey() );
    }
  }

  /*!
   * \return The key associated with this node
   */
  inline const TKey& getKey() const
  {
    return ( mKey );
  }

  /*!
   * \return The key associated with this node
   */
  inline bool isInvalid() const
  {
    return ( mTree == nullptr );
  }

  /*!
   * \return True, if this node is treated as a leaf
   */
  inline bool isLeaf() const
  {
    return ( mIsLeaf );
  }

private:
  static std::size_t NodeCount;

private:
  Tree* mTree;

  ChildContainer mChildren;

  bool mIsLeaf;
  TKey mKey;
  TValue mValue;
};


//! Each template instance should count its own nodes
template< typename TKey, typename TValue >
std::size_t abNode<TKey, TValue>::NodeCount = 0;

#endif // ABTREE_HPP
