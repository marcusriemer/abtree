#include <ios>
#include <iostream>

#include "abtree.hpp"
#include "testrunner.hpp"

int main( int argc, char** argv )
{
  // We want to print pretty booleans
  std::cout.setf( std::ios::boolalpha );

  typedef abTree<int, std::string> IntStringTree;
  TestRunner runner( std::cout, std::cin );

  return ( 0 );
}
