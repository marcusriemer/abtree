#ifndef TESTRUNNER_HPP
#define TESTRUNNER_HPP

#include <iosfwd>

/*!
 * \brief Reads a list of operations and to perform benchmarking
 * Yes, this code is ugly ...
 */
class TestRunner
{
public:
  /*!
   * \brief Construct the runner and do some tests
   * \param out The stream to read from
   * \param in The stream to write to
   */
  TestRunner( std::ostream& out, std::istream& in );

private:
  std::ostream& mOut;
  std::istream& mIn;
};

#endif // TESTRUNNER_HPP
