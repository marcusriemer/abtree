#include "testrunner.hpp"

#include <string>
#include <iostream>
#include <vector>
#include <chrono>

#include <boost/algorithm/string/replace.hpp>

#include "abtree.hpp"

TestRunner::TestRunner( std::ostream &out, std::istream &in )
  : mOut( out )
  , mIn( in )
{
  typedef abTree<int, int> Tree;
  Tree* intStringTree = nullptr;
  std::chrono::time_point<std::chrono::high_resolution_clock> start;

  std::string line;
  while( std::getline( mIn, line, '\n' ) )
  {
    std::size_t firstComma = line.find( ',' );
    std::string verb = line.substr( 0, firstComma );

    if( verb == "create" )
    {
      // Delete previous tree
      delete intStringTree;

      std::size_t secondComma = line.find( ',', firstComma + 1 );
      std::string a = line.substr( firstComma + 1, line.size() - secondComma - 1 );
      std::string b = line.substr( secondComma + 1 );

      intStringTree = new Tree( std::stoi( a ), std::stoi( b ) );
    }
    else if( verb == "insert" )
    {
      std::size_t secondComma = line.find( ',', firstComma + 1 );
      std::string key = line.substr( firstComma + 1, secondComma );
      std::string value = line.substr( secondComma + 1 );

      intStringTree->insert( std::stoi( key ),  std::stoi(value) );
    }
    else if( verb == "print" )
    {
      intStringTree->print( out );
    }
    else if( verb == "echo" )
    {
      std::string text = line.substr( firstComma + 1 );
      boost::replace_all(text, "\\n", "\n");

      mOut << text << std::flush;
    }
    else if( verb == "timer" )
    {
      std::string action = line.substr( firstComma + 1 );

      if( action == "start" )
      {
        start = std::chrono::high_resolution_clock::now();
      }
      else if( action == "print" )
      {
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        std::cout << elapsed.count() << std::endl;
      }
    }
    else if( verb == "contains" )
    {
      std::size_t secondComma = line.find( ',', firstComma + 1 );
      std::string key = line.substr( firstComma + 1, secondComma - firstComma - 1 );
      std::string expectedStr = line.substr( secondComma + 1 );
      bool expected = false;
      if (expectedStr == "True" || expectedStr == "true")
      {
        expected = true;
      }


      bool contained = intStringTree->contains( std::stoi( key ) );

      if (expected != contained)
      {
        std::cout << "Error: contains(" << key << ") => "
          << intStringTree->contains( std::stoi( key ) )
          << ", expected " << expected << std::endl;
      }

    }
    else if( verb == "value" )
    {
      std::size_t secondComma = line.find( ',', firstComma + 1 );
      std::string key = line.substr( firstComma + 1, line.size() - secondComma - 1 );
      std::string value = line.substr( secondComma + 1 );

      bool contained = intStringTree->contains( std::stoi( key ) );
      std::cout << "value(" << key << ") == " << value << " => "
        << contained << std::endl;
    }
    else if( verb == "remove" )
    {
      std::string key = line.substr( firstComma + 1 );
      intStringTree->erase( std::stoi( key ) );
    }
    else if( verb == "size" )
    {
      const abTreeStats stats = intStringTree->stats( );

      /*std::cout << "size() => "
        << stats.numInnerNodes << " inner nodes, "
        << stats.numLeafs << " leafs" << std::endl;*/
    }
  }
}
